package mx.com.isban.beans.contratos.estatuscontratos;

public class BeanEstatusContratosDAO {
	
	/*
	 * 
	 * Se agregan los datos de entrada de las transacciones :
	 * 
	 * 
	 * 25
	 * 
	 * 
	 *  codEstat
		DESESTAT
		INDOPERA
		INDBLOQ
		INDCANCE
		INDBLOLE
		INDINVER
		INDDETAL
		INDBLQBL
		INDCANBL
		INDVALDO

	 * */

	private String codEstat;
	private String desEstat;
	private String indOpera;
	private String indBloq;
	private String indCance;
	private String indBloLe;
	private String IndInver;
	private String indDetal;
	private String indBlqble;
	private String indCanbl;
	private String indValdo;
	
	public String getcodEstat() {
		return codEstat;
	}
	public void setcodEstat(String codEstat) {
		this.codEstat = codEstat;
	}
	public String getDesEstat() {
		return desEstat;
	}
	public void setDesEstat(String desEstat) {
		this.desEstat = desEstat;
	}
	public String getIndOpera() {
		return indOpera;
	}
	public void setIndOpera(String indOpera) {
		this.indOpera = indOpera;
	}
	public String getIndBloq() {
		return indBloq;
	}
	public void setIndBloq(String indBloq) {
		this.indBloq = indBloq;
	}
	public String getIndCance() {
		return indCance;
	}
	public void setIndCance(String indCance) {
		this.indCance = indCance;
	}
	public String getIndBloLe() {
		return indBloLe;
	}
	public void setIndBloLe(String indBloLe) {
		this.indBloLe = indBloLe;
	}
	public String getIndInver() {
		return IndInver;
	}
	public void setIndInver(String indInver) {
		IndInver = indInver;
	}
	public String getIndDetal() {
		return indDetal;
	}
	public void setIndDetal(String indDetal) {
		this.indDetal = indDetal;
	}
	public String getIndBlqble() {
		return indBlqble;
	}
	public void setIndBlqble(String indBlqble) {
		this.indBlqble = indBlqble;
	}
	public String getIndCanbl() {
		return indCanbl;
	}
	public void setIndCanbl(String indCanbl) {
		this.indCanbl = indCanbl;
	}
	public String getIndValdo() {
		return indValdo;
	}
	public void setIndValdo(String indValdo) {
		this.indValdo = indValdo;
	}

	
	
	
}
