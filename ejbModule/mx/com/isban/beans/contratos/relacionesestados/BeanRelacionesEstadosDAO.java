package mx.com.isban.beans.contratos.relacionesestados;

public class BeanRelacionesEstadosDAO {

	/*
	 * Transacciónes :
	 * 
	 * 
	 * MI20	TRX MODIFCACIÓN ESTADOS
	 * 
	 * */
	
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private int idEmpr;
	private String idCent;
	private int idContr;
	private String idProd;
	private String idStiPro;
	private String peCalPar;
	private String peNumPer;
	private String codDocum;
	private String codEstNu;
	private String desComen;
	private String FecEstaD;
	
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public int getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(int idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public int getIdContr() {
		return idContr;
	}
	public void setIdContr(int idContr) {
		this.idContr = idContr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getPeCalPar() {
		return peCalPar;
	}
	public void setPeCalPar(String peCalPar) {
		this.peCalPar = peCalPar;
	}
	public String getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}
	public String getCodDocum() {
		return codDocum;
	}
	public void setCodDocum(String codDocum) {
		this.codDocum = codDocum;
	}
	public String getCodEstNu() {
		return codEstNu;
	}
	public void setCodEstNu(String codEstNu) {
		this.codEstNu = codEstNu;
	}
	public String getdesComen() {
		return desComen;
	}
	public void setdesComen(String desComen) {
		desComen = desComen;
	}
	public String getFecEstaD() {
		return FecEstaD;
	}
	public void setFecEstaD(String fecEstaD) {
		FecEstaD = fecEstaD;
	}

	
	
	
	
}
