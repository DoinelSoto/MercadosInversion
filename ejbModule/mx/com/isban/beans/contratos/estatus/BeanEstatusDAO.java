package mx.com.isban.beans.contratos.estatus;

public class BeanEstatusDAO {
	
	/*
	 * Datos de entrada para las transacciónes :
	 * 
	 *  MI25	TRX ALTA ESTATUS
		MI26	TRX BAJA ESTATUS
	    MI27	TRX MODIFICACIÓN ESTATUS

	 * 
	 * */

	private String codEstat;
	private String desEstat;
	private String indOpera;
	private String indBloq;
	private String indCance;
	private String indBloLe;
	private String indInver;
	private String indDetal;
	private String indBlqble;
	private String indCanbl;
	private String indValdo;
	
	public String getCodEstat() {
		return codEstat;
	}
	public void setCodEstat(String codEstat) {
		this.codEstat = codEstat;
	}
	public String getDesEstat() {
		return desEstat;
	}
	public void setDesEstat(String desEstat) {
		this.desEstat = desEstat;
	}
	public String getIndOpera() {
		return indOpera;
	}
	public void setIndOpera(String indOpera) {
		this.indOpera = indOpera;
	}
	public String getIndBloq() {
		return indBloq;
	}
	public void setIndBloq(String indBloq) {
		this.indBloq = indBloq;
	}
	public String getIndCance() {
		return indCance;
	}
	public void setIndCance(String indCance) {
		this.indCance = indCance;
	}
	public String getIndBloLe() {
		return indBloLe;
	}
	public void setIndBloLe(String indBloLe) {
		this.indBloLe = indBloLe;
	}
	public String getIndInver() {
		return indInver;
	}
	public void setIndInver(String indInver) {
		this.indInver = indInver;
	}
	public String getIndDetal() {
		return indDetal;
	}
	public void setIndDetal(String indDetal) {
		this.indDetal = indDetal;
	}
	public String getIndBlqble() {
		return indBlqble;
	}
	public void setIndBlqble(String indBlqble) {
		this.indBlqble = indBlqble;
	}
	public String getIndCanbl() {
		return indCanbl;
	}
	public void setIndCanbl(String indCanbl) {
		this.indCanbl = indCanbl;
	}
	public String getIndValdo() {
		return indValdo;
	}
	public void setIndValdo(String indValdo) {
		this.indValdo = indValdo;
	}
	
	
	
	
}
