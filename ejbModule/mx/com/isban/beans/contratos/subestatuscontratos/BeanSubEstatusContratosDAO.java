package mx.com.isban.beans.contratos.subestatuscontratos;

public class BeanSubEstatusContratosDAO {
	
	/*
	 * Se agregan los datos de entrada para las transacciones :
	 * 
	 * 28:
	 * codEstat
	   codSubEs
       desSubEs

	 * 29 :
	 * CODESTAT
	   CODSUBES

	 * 
	 * 
	 * 
	 * */

	
	private String codSubEs;
    private String desSubEs;
    
    private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String codEstat;
	private String inDrella;
	private String codEstRe;
	private String codSubRe;
	
	
	
    
	public String getCodEstat() {
		return codEstat;
	}
	public void setCodEstat(String codEstat) {
		this.codEstat = codEstat;
	}
	public String getCodSubEs() {
		return codSubEs;
	}
	public void setCodSubEs(String codSubEs) {
		this.codSubEs = codSubEs;
	}
	public String getdesSubEs() {
		return desSubEs;
	}
	public void setdesSubEs(String desSubEs) {
		this.desSubEs = desSubEs;
	}
	
	
}
