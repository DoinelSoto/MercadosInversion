package mx.com.isban.beans.contratos.contrato;

public class BeanContratosDAO {

	/**
	 * Se implementan los datos de entrada para transacciones  :
	 * 
	 *  MI16	TRX CONSULTA INVERSION
		MI18	TRX ALTA CONTRATO INVERSION
		MI19	TRX CAMBIO DE ESTATUS DE CONTRATO

	 * 
	 * */
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String tipConsu;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String idEmprCo;
	private String desAlias;
	private String codEstad;
	private String codDetEs;
	private String desDetSe;
	private String feCambio;
	
	private String idSprod;
	private String indInInt;
	private String indInCon;
	private String indInUso;
	private String indInRel;
	private String indInRef;
	private String fecConsu;
	
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getTipConsu() {
		return tipConsu;
	}
	public void setTipConsu(String tipConsu) {
		this.tipConsu = tipConsu;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public String getIdContr() {
		return idContr;
	}
	public void setIdContr(String idContr) {
		this.idContr = idContr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getIdEmprCo() {
		return idEmprCo;
	}
	public void setIdEmprCo(String idEmprCo) {
		this.idEmprCo = idEmprCo;
	}
	public String getDesAlias() {
		return desAlias;
	}
	public void setDesAlias(String desAlias) {
		this.desAlias = desAlias;
	}
	public String getCodEstad() {
		return codEstad;
	}
	public void setCodEstad(String codEstad) {
		this.codEstad = codEstad;
	}
	public String getCodDetEs() {
		return codDetEs;
	}
	public void setCodDetEs(String codDetEs) {
		this.codDetEs = codDetEs;
	}
	public String getDesDetSe() {
		return desDetSe;
	}
	public void setDesDetSe(String desDetSe) {
		this.desDetSe = desDetSe;
	}
	public String getFeCambio() {
		return feCambio;
	}
	public void setFeCambio(String feCambio) {
		this.feCambio = feCambio;
	}
	public String getIdSprod() {
		return idSprod;
	}
	public void setIdSprod(String idSprod) {
		this.idSprod = idSprod;
	}
	public String getIndInInt() {
		return indInInt;
	}
	public void setIndInInt(String indInInt) {
		this.indInInt = indInInt;
	}
	public String getIndInCon() {
		return indInCon;
	}
	public void setIndInCon(String indInCon) {
		this.indInCon = indInCon;
	}
	public String getIndInUso() {
		return indInUso;
	}
	public void setIndInUso(String indInUso) {
		this.indInUso = indInUso;
	}
	public String getIndInRel() {
		return indInRel;
	}
	public void setIndInRel(String indInRel) {
		this.indInRel = indInRel;
	}
	public String getIndInRef() {
		return indInRef;
	}
	public void setIndInRef(String indInRef) {
		this.indInRef = indInRef;
	}
	public String getFecConsu() {
		return fecConsu;
	}
	public void setFecConsu(String fecConsu) {
		this.fecConsu = fecConsu;
	}

	
	
	
	
}
