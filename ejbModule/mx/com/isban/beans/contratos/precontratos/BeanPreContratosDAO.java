package mx.com.isban.beans.contratos.precontratos;

public class BeanPreContratosDAO {

	
	/*
	 * Involucra las transacciones :
	 * 
	 *      MI11	TRX ALTA PRE-CONTRATO GLOBAL
	     	MI14	TRX LISTA PRE-CONTRATO

	 * 
	 * 
	 * **/
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String idEmpr;
	private String idProd;
	private String idStiPro;
	private String codMonSw;
	private String buC;
	private String codEjecu;
	private String idBanca;
	private String idEmprCo;
	private String codCenCo;
	private String idTipCue;
	private String idTipCon;
	private String indAuPCC;
	private String indAuPCF;
	private String inDauCFI;
	private String indDiscr;
	private String indTiCuD;
	private int impMaxOp;
	private String divMaxOp;
	private String IndEnCoC;
	private String IndCoAgr;
	private String idEmprGr;
	private String idCentGr;
	private int idContGr;
	private String idProdGr;
	private String idEmpRes;
	private String idCentEs;
	private int IdContEs;
	private String codTipSe;
	private String codAfi;
	private int secDomPr;
	private int secDomEn;
	private int secDomFi;
	
	private int numFolio;
	private int peNumPer;
	private int fecApert;
	private int indRella;
	private int nFoRella;
	
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getCodMonSw() {
		return codMonSw;
	}
	public void setCodMonSw(String codMonSw) {
		this.codMonSw = codMonSw;
	}
	public String getBuC() {
		return buC;
	}
	public void setBuC(String buC) {
		this.buC = buC;
	}
	public String getCodEjecu() {
		return codEjecu;
	}
	public void setCodEjecu(String codEjecu) {
		this.codEjecu = codEjecu;
	}
	public String getIdBanca() {
		return idBanca;
	}
	public void setIdBanca(String idBanca) {
		this.idBanca = idBanca;
	}
	public String getIdEmprCo() {
		return idEmprCo;
	}
	public void setIdEmprCo(String idEmprCo) {
		this.idEmprCo = idEmprCo;
	}
	public String getCodCenCo() {
		return codCenCo;
	}
	public void setCodCenCo(String codCenCo) {
		this.codCenCo = codCenCo;
	}
	public String getIdTipCue() {
		return idTipCue;
	}
	public void setIdTipCue(String idTipCue) {
		this.idTipCue = idTipCue;
	}
	public String getIdTipCon() {
		return idTipCon;
	}
	public void setIdTipCon(String idTipCon) {
		this.idTipCon = idTipCon;
	}
	public String getIndAuPCC() {
		return indAuPCC;
	}
	public void setIndAuPCC(String indAuPCC) {
		this.indAuPCC = indAuPCC;
	}
	public String getIndAuPCF() {
		return indAuPCF;
	}
	public void setIndAuPCF(String indAuPCF) {
		this.indAuPCF = indAuPCF;
	}
	public String getInDauCFI() {
		return inDauCFI;
	}
	public void setInDauCFI(String inDauCFI) {
		this.inDauCFI = inDauCFI;
	}
	public String getIndDiscr() {
		return indDiscr;
	}
	public void setIndDiscr(String indDiscr) {
		this.indDiscr = indDiscr;
	}
	public String getIndTiCuD() {
		return indTiCuD;
	}
	public void setIndTiCuD(String indTiCuD) {
		this.indTiCuD = indTiCuD;
	}
	public int getImpMaxOp() {
		return impMaxOp;
	}
	public void setImpMaxOp(int impMaxOp) {
		this.impMaxOp = impMaxOp;
	}
	public String getDivMaxOp() {
		return divMaxOp;
	}
	public void setDivMaxOp(String divMaxOp) {
		this.divMaxOp = divMaxOp;
	}
	public String getIndEnCoC() {
		return IndEnCoC;
	}
	public void setIndEnCoC(String indEnCoC) {
		IndEnCoC = indEnCoC;
	}
	public String getIndCoAgr() {
		return IndCoAgr;
	}
	public void setIndCoAgr(String indCoAgr) {
		IndCoAgr = indCoAgr;
	}
	public String getIdEmprGr() {
		return idEmprGr;
	}
	public void setIdEmprGr(String idEmprGr) {
		this.idEmprGr = idEmprGr;
	}
	public String getIdCentGr() {
		return idCentGr;
	}
	public void setIdCentGr(String idCentGr) {
		this.idCentGr = idCentGr;
	}
	public int getIdContGr() {
		return idContGr;
	}
	public void setIdContGr(int idContGr) {
		this.idContGr = idContGr;
	}
	public String getIdProdGr() {
		return idProdGr;
	}
	public void setIdProdGr(String idProdGr) {
		this.idProdGr = idProdGr;
	}
	public String getidEmpRes() {
		return idEmpRes;
	}
	public void setidEmpRes(String idEmpRes) {
		this.idEmpRes = idEmpRes;
	}
	public String getIdCentEs() {
		return idCentEs;
	}
	public void setIdCentEs(String idCentEs) {
		this.idCentEs = idCentEs;
	}
	public int getIdContEs() {
		return IdContEs;
	}
	public void setIdContEs(int idContEs) {
		IdContEs = idContEs;
	}
	public String getCodTipSe() {
		return codTipSe;
	}
	public void setCodTipSe(String codTipSe) {
		this.codTipSe = codTipSe;
	}
	public String getCodAfi() {
		return codAfi;
	}
	public void setCodAfi(String codAfi) {
		this.codAfi = codAfi;
	}
	public int getSecDomPr() {
		return secDomPr;
	}
	public void setSecDomPr(int secDomPr) {
		this.secDomPr = secDomPr;
	}
	public int getSecDomEn() {
		return secDomEn;
	}
	public void setSecDomEn(int secDomEn) {
		this.secDomEn = secDomEn;
	}
	public int getSecDomFi() {
		return secDomFi;
	}
	public void setSecDomFi(int secDomFi) {
		this.secDomFi = secDomFi;
	}
	public int getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(int numFolio) {
		this.numFolio = numFolio;
	}
	public int getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(int peNumPer) {
		this.peNumPer = peNumPer;
	}
	public int getFecApert() {
		return fecApert;
	}
	public void setFecApert(int fecApert) {
		this.fecApert = fecApert;
	}
	public int getIndRella() {
		return indRella;
	}
	public void setIndRella(int indRella) {
		this.indRella = indRella;
	}
	public int getnFoRella() {
		return nFoRella;
	}
	public void setnFoRella(int nFoRella) {
		this.nFoRella = nFoRella;
	}

	
	
}
