package mx.com.isban.beans.contratos.contratoinversion;

/*
 * Se añaden los datos de entrada para las transacciones :
 * 
 * 30 : 
 * 
 * COIDEMPR
CANALOPE
CANALCOM
TIPCONSU
IDEMPR
IDCENT
IDCONTR
IDPROD
IDSTIPRO
IDEMPRCO
DESALIAS
PENUMPER
PESECDO1
PESECDO2
PESECDO3

 * 31:
 * 
 * COIDEMPR
CANALOPE
CANALCOM
TIPCONSU
IDEMPR
IDCENT
IDCONTR
IDPROD
IDSTIPRO
IDEMPRCO
DESALIAS
IDBANCA
IDTIPCUE
IDTIPCON
INDAUPCC
INDAUPCF
INDAUCFI
indDiscr
INDTICUD
indTiCuR
InDestCu
impMaxOp
divMaxOp
IndEnCoC
IndCoAgr
IDEMPRAG
idCentAg
idContAg
idProdAg
idStipAg
indCoEsp
idEmpReS
IDCENTES
IdContEs
IDPRODES
idStiPES
CODTIPSE
CODAFI
desContr
desFirma
desAreaR
DESREFEX

 * 
 * 
 * 
 * 
 * */

public class BeanContratoInversionDAO {
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String tipConsu;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String idEmprCo;
	private String desAlias;
	private String peNumPer;
	private int peSecDo1;
	private int  peSecDo2;
	private int  peSecDo3;
	// datos de entrada extra para la transacción 31 
	private String idBanca;
	private String idTipCue;
	private String idTipCon;
	private String IndAuPCC;
	private String indAuPCF;
	private String inDauCFI;
	private String inDauSCR;
	private String indTiCuD;
	private String indTiCuR;
	private String InDestCu;
	private int impMaxOp;
	private String divMaxOp;
	private String IndEnCoC;
	private String IndCoAgr;
	private String idEmprAG;
	private String idCentAg;
	private String idContAg;
	private String idProdAg;
	private String idStipAg;
	private String indCoEsp;
	private String idEmpReS;
	private String idCentES;
	private String IdContEs;
	private String 	idProdEs;
	private String 	idStipEs;
	private String codTipSe;
	private String codAfi;
	private String desContr;
	private String desFirma;
	private String desAreaR;
	private String DesRefEx;
	
	
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getTipConsu() {
		return tipConsu;
	}
	public void setTipConsu(String tipConsu) {
		this.tipConsu = tipConsu;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public String getIdContr() {
		return idContr;
	}
	public void setIdContr(String idContr) {
		this.idContr = idContr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getIdEmprCo() {
		return idEmprCo;
	}
	public void setIdEmprCo(String idEmprCo) {
		this.idEmprCo = idEmprCo;
	}
	public String getDesAlias() {
		return desAlias;
	}
	public void setDesAlias(String desAlias) {
		this.desAlias = desAlias;
	}
	public String getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}
	public int getPeSecDo1() {
		return peSecDo1;
	}
	public void setPeSecDo1(int peSecDo1) {
		this.peSecDo1 = peSecDo1;
	}
	public int getPeSecDo2() {
		return peSecDo2;
	}
	public void setPeSecDo2(int peSecDo2) {
		this.peSecDo2 = peSecDo2;
	}
	public int getPeSecDo3() {
		return peSecDo3;
	}
	public void setPeSecDo3(int peSecDo3) {
		this.peSecDo3 = peSecDo3;
	}
	public String getIdBanca() {
		return idBanca;
	}
	public void setIdBanca(String idBanca) {
		this.idBanca = idBanca;
	}
	public String getIdTipCue() {
		return idTipCue;
	}
	public void setIdTipCue(String idTipCue) {
		this.idTipCue = idTipCue;
	}
	public String getIdTipCon() {
		return idTipCon;
	}
	public void setIdTipCon(String idTipCon) {
		this.idTipCon = idTipCon;
	}
	public String getIndAuPCC() {
		return IndAuPCC;
	}
	public void setIndAuPCC(String indAuPCC) {
		IndAuPCC = indAuPCC;
	}
	public String getIndAuPCF() {
		return indAuPCF;
	}
	public void setIndAuPCF(String indAuPCF) {
		this.indAuPCF = indAuPCF;
	}
	public String getInDauCFI() {
		return inDauCFI;
	}
	public void setInDauCFI(String inDauCFI) {
		this.inDauCFI = inDauCFI;
	}
	public String getInDauSCR() {
		return inDauSCR;
	}
	public void setInDauSCR(String inDauSCR) {
		this.inDauSCR = inDauSCR;
	}
	public String getIndTiCuD() {
		return indTiCuD;
	}
	public void setIndTiCuD(String indTiCuD) {
		this.indTiCuD = indTiCuD;
	}
	public String getIndTiCuR() {
		return indTiCuR;
	}
	public void setIndTiCuR(String indTiCuR) {
		this.indTiCuR = indTiCuR;
	}
	public String getInDestCu() {
		return InDestCu;
	}
	public void setInDestCu(String inDestCu) {
		InDestCu = inDestCu;
	}
	public int getImpMaxOp() {
		return impMaxOp;
	}
	public void setImpMaxOp(int impMaxOp) {
		this.impMaxOp = impMaxOp;
	}
	public String getDivMaxOp() {
		return divMaxOp;
	}
	public void setDivMaxOp(String divMaxOp) {
		this.divMaxOp = divMaxOp;
	}
	public String getIndEnCoC() {
		return IndEnCoC;
	}
	public void setIndEnCoC(String indEnCoC) {
		IndEnCoC = indEnCoC;
	}
	public String getIndCoAgr() {
		return IndCoAgr;
	}
	public void setIndCoAgr(String indCoAgr) {
		IndCoAgr = indCoAgr;
	}
	public String getIdEmprAG() {
		return idEmprAG;
	}
	public void setIdEmprAG(String idEmprAG) {
		this.idEmprAG = idEmprAG;
	}
	public String getidCentAg() {
		return idCentAg;
	}
	public void setidCentAg(String idCentAg) {
		this.idCentAg = idCentAg;
	}
	public String getIdContAg() {
		return idContAg;
	}
	public void setIdContAg(String idContAg) {
		this.idContAg = idContAg;
	}
	public String getidProdAg() {
		return idProdAg;
	}
	public void setidProdAg(String idProdAg) {
		this.idProdAg = idProdAg;
	}
	public String getIdStipAg() {
		return idStipAg;
	}
	public void setIdStipAg(String idStipAg) {
		this.idStipAg = idStipAg;
	}
	public String getIndCoEsp() {
		return indCoEsp;
	}
	public void setIndCoEsp(String indCoEsp) {
		this.indCoEsp = indCoEsp;
	}
	public String getidEmpReS() {
		return idEmpReS;
	}
	public void setidEmpReS(String idEmpReS) {
		this.idEmpReS = idEmpReS;
	}
	public String getIdCentES() {
		return idCentES;
	}
	public void setIdCentES(String idCentES) {
		this.idCentES = idCentES;
	}
	public String getIdContEs() {
		return IdContEs;
	}
	public void setIdContEs(String idContEs) {
		IdContEs = idContEs;
	}
	public String getIdProdEs() {
		return idProdEs;
	}
	public void setIdProdEs(String idProdEs) {
		this.idProdEs = idProdEs;
	}
	public String getIdStipEs() {
		return idStipEs;
	}
	public void setIdStipEs(String idStipEs) {
		this.idStipEs = idStipEs;
	}
	public String getCodTipSe() {
		return codTipSe;
	}
	public void setCodTipSe(String codTipSe) {
		this.codTipSe = codTipSe;
	}
	public String getCodAfi() {
		return codAfi;
	}
	public void setCodAfi(String codAfi) {
		this.codAfi = codAfi;
	}
	public String getDesContr() {
		return desContr;
	}
	public void setDesContr(String desContr) {
		this.desContr = desContr;
	}
	public String getDesFirma() {
		return desFirma;
	}
	public void setDesFirma(String desFirma) {
		this.desFirma = desFirma;
	}
	public String getDesAreaR() {
		return desAreaR;
	}
	public void setDesAreaR(String desAreaR) {
		this.desAreaR = desAreaR;
	}
	public String getDesRefEx() {
		return DesRefEx;
	}
	public void setDesRefEx(String desRefEx) {
		DesRefEx = desRefEx;
	}

	
	
	
}
