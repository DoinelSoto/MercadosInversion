package mx.com.isban.beans.contratos.contratos;

public class BeanContratosDAO {

	/*
	 * transacciones :
	 * 
	 *  MI21	TRX CAMBIO CONTRATO
		MI22	TRX CAMBIO CONTRATO
MI24	TRX MODIFICACIÓN CONTRATO

	 * 
	 * */
	
	private String coidEmpr;  
	private String canalOpe;  
	private String canalCom;
	private String tipConsu;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;	
	private String idStiPro;
	private String idEmprCo;
	private String desAlias;
	private String nusAlias;
	private String peNumPer;
	private String peCalPar;
	private String codDocum;
	private String codEstNu;
	private String desComen;
	private String FecEstaD;
	
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getTipConsu() {
		return tipConsu;
	}
	public void setTipConsu(String tipConsu) {
		this.tipConsu = tipConsu;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public String getIdContr() {
		return idContr;
	}
	public void setIdContr(String idContr) {
		this.idContr = idContr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getIdEmprCo() {
		return idEmprCo;
	}
	public void setIdEmprCo(String idEmprCo) {
		this.idEmprCo = idEmprCo;
	}
	public String getDesAlias() {
		return desAlias;
	}
	public void setDesAlias(String desAlias) {
		this.desAlias = desAlias;
	}
	public String getNusAlias() {
		return nusAlias;
	}
	public void setNusAlias(String nusAlias) {
		this.nusAlias = nusAlias;
	}
	public String getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}
	public String getPeCalPar() {
		return peCalPar;
	}
	public void setPeCalPar(String peCalPar) {
		this.peCalPar = peCalPar;
	}
	public String getCodDocum() {
		return codDocum;
	}
	public void setCodDocum(String codDocum) {
		this.codDocum = codDocum;
	}
	public String getCodEstNu() {
		return codEstNu;
	}
	public void setCodEstNu(String codEstNu) {
		this.codEstNu = codEstNu;
	}
	public String getDesComen() {
		return desComen;
	}
	public void setDesComen(String desComen) {
		this.desComen = desComen;
	}
	public String getFecEstaD() {
		return FecEstaD;
	}
	public void setFecEstaD(String fecEstaD) {
		FecEstaD = fecEstaD;
	}
	
	
	
	
	
	
}
