package mx.com.isban.beans.contratos.documentos;

public class beanDocumentosDAO {
	
	/*
	 * transacciones :
	 * 
	 * MI17	TRX CONSULTA DE DOCUMENTOS DEL  CONTRATO
	 * 
	 * */


	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String peCalPar;
	private String peNumPer;
	private String codDocum;
	private String inDrella;
	private String idEmprR;
	private String idCentR;
	private String idContrR;
	private String idProdRR;
	private String idStiProR;
	private String peCalPaL;
	private String peNumPeL;
	private String codDocuR;
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public String getIdContr() {
		return idContr;
	}
	public void setIdContr(String idContr) {
		this.idContr = idContr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getPeCalPar() {
		return peCalPar;
	}
	public void setPeCalPar(String peCalPar) {
		this.peCalPar = peCalPar;
	}
	public String getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}
	public String getCodDocum() {
		return codDocum;
	}
	public void setCodDocum(String codDocum) {
		this.codDocum = codDocum;
	}
	public String getInDrella() {
		return inDrella;
	}
	public void setInDrella(String inDrella) {
		this.inDrella = inDrella;
	}
	public String getIdEmprR() {
		return idEmprR;
	}
	public void setIdEmprR(String idEmprR) {
		this.idEmprR = idEmprR;
	}
	public String getIdCentR() {
		return idCentR;
	}
	public void setIdCentR(String idCentR) {
		this.idCentR = idCentR;
	}
	public String getIdContrR() {
		return idContrR;
	}
	public void setIdContrR(String idContrR) {
		this.idContrR = idContrR;
	}
	public String getIdProdRR() {
		return idProdRR;
	}
	public void setIdProdRR(String idProdRR) {
		this.idProdRR = idProdRR;
	}
	public String getIdStiProR() {
		return idStiProR;
	}
	public void setIdStiProR(String idStiProR) {
		this.idStiProR = idStiProR;
	}
	public String getPeCalPaL() {
		return peCalPaL;
	}
	public void setPeCalPaL(String peCalPaL) {
		this.peCalPaL = peCalPaL;
	}
	public String getPeNumPeL() {
		return peNumPeL;
	}
	public void setPeNumPeL(String peNumPeL) {
		this.peNumPeL = peNumPeL;
	}
	public String getCodDocuR() {
		return codDocuR;
	}
	public void setCodDocuR(String codDocuR) {
		this.codDocuR = codDocuR;
	}
	
	
	
}
