package mx.isban.beans.contratos.contratosagrupados;

public class contratosAgrupados {

	/*
	 * Datos de entrada para las transacciones :
	 * 
	 * 
	 *  MI30	TRX Cambio de titular del Contrato
		MI31	TRX Mantenimiento de datos generales
		MI32	TRX Lista contrato agrupador
		MI33	TRX Alta general contrato agrupador
		MI34	TRX Lista contratos agrupados
		MI35	TRX Alta contratos agrupados
		MI36	TRX Baja contratos agrupados
		MI37	TRX opción Paperless
		MI38	TRX Estatus “Actualización de Documentos” por documentación próxima a su vencimiento

	 * 
	 * 
	 * */
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String inDrella;
	private String idEmpRe;
	private String idCentRe;
	private String idContRe;
	private String idProdRe;
	private String idStiProE;
	private String tipConsu;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String idEmprCo;
	private String desAlias;
	private String idBanca;
	private String codEjecu;
	private String peNumTel;
	private String codMonSw;
	private String nomGrupo;
	private String desGrupo;
	private String indTiGru;
	private String idGruUsu;
	private String feComVal;
	private String feFinVal;
	private String indUsoGr;
	private String idTipCue;
	private String idTipCon;
	private String indAuPCC;
	private String indAuPCF;
	private String inDauCFI;
	private String inDauSCR;
	private String indTiCuD;
	private String indTiCuR;
	private String InDestCu;
	private int impMaxOp;
	private String divMaxOp;
	private String IndEnCoC;	
	private String idEmprAG;
	private String idCentAg;
	private String idContAg;
	private String idProdAg;
	private String idStipAg;
	private String indCoEsp;
	private String idEmpReS;
	private String idCentES;
	private String IdContEs;
	private String 	idProdEs;
	private String 	idStipEs;
	private String codTipSe;
	private String codAfi;
	private String desContr;
	private String desFirma;
	private String desAreaR;
	private String DesRefEx;
	private String IndCoAgr;
	private String tipAgrup;
	private String idContrAg;
	private String idStiPrAg;
	private String desAliaG;
	private String peNumPer;
	private int peSecDo1;
	private int  peSecDo2;
	private int  peSecDo3;
	private String  fecha;
	private String  fechaPro;
	
	
	
	
}
